#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "LinkList.h"

StudentList* Insert_ToHead(StudentList *First, StudentInfo node){
	StudentList* TempNode;
	TempNode = (StudentList*) malloc(sizeof(StudentList));
	TempNode->_node = node;
	TempNode->_next = First;
	First = TempNode;
	return First;
}

StudentList* Insert_Next(StudentList* Pred, StudentInfo node){
	StudentList *TempNode;
	TempNode= (StudentList*)malloc(sizeof(StudentList));
	TempNode->_node = node;
	TempNode->_next = Pred->_next;
	Pred->_next = TempNode;
	return TempNode;
}

StudentList* Insert_Middle(StudentList * First,StudentInfo node){
	StudentList * TempNode;
	StudentList* Pred;
	TempNode = First;
	while(TempNode!=NULL){
		if(strcmp(node._firstName,TempNode->_node._firstName) < 0 ){
			TempNode = Insert_Next(TempNode,node);
			return First;
		}
		Pred = TempNode;
		TempNode = TempNode->_next;
	}
	Pred = Insert_Next(Pred,node);
	return First;
}

StudentList* Delete_Head(StudentList* First){
	StudentList* TempNode;
	TempNode = First->_next;
	free(First);
	return TempNode;
}
StudentList* Delete_Middle(StudentList* First,char* str){
	StudentList* TempNode;
	StudentList* Pred;
	TempNode = First;
	while(TempNode != NULL){

		if(strcmp(TempNode->_node._id,str)==0){
			printf("Delete Score Of Student has StudentID : %s",str );
			if(TempNode == First){
				return Delete_Head(First);
			}
			Delete(Pred);
			return First;
		}
		Pred = TempNode;
		TempNode = TempNode->_next;
	}
}
StudentInfo Delete(StudentList *Pred){
	StudentInfo node;
	StudentList *TempNode;
	TempNode = Pred->_next;
	Pred->_next = Pred->_next->_next;
	node = TempNode->_node;
	free(TempNode);
	return node;
}
StudentInfo Search(StudentList * First,char* str){
	StudentList* TempNode;
	TempNode = First;
	while(TempNode !=NULL){
		if(strcmp(TempNode->_node._id,str) == 0){
			return TempNode->_node;
		}
		TempNode = TempNode->_next;
	}
}
StudentInfo Max(StudentList* First,int _FMid){
	StudentList* TempNode;
	TempNode = First;
	float max_score = 0;
	StudentInfo node;
	while(TempNode!=NULL){
		if(Average_Score(TempNode->_node,_FMid) > max_score){
			max_score = Average_Score(TempNode->_node,_FMid);
			node = TempNode->_node;
		}
		TempNode = TempNode->_next;
	}
	return node;
}	
StudentInfo Min(StudentList* First,int _FMid){
	StudentList* TempNode;
	TempNode = First;
	float min_score = 10;
	StudentInfo node;
	while(TempNode!=NULL){
		if(Average_Score(TempNode->_node,_FMid) < min_score){
			min_score = Average_Score(TempNode->_node,_FMid);
			node = TempNode->_node;
		}
		TempNode = TempNode->_next;
	}
	return node;
}	
float Average_Score(StudentInfo node, int _FMid){
	return ((node._midScore* _FMid + node._finalScore* (100 - _FMid))/100);
}
float Total_Average_Mark(StudentList* First, int _FMid){
	StudentList*TempNode;
	TempNode = First;
	int count = 0;
	float sum =0;
	while(TempNode!=NULL){
		count++;
		sum+= Average_Score(TempNode->_node,_FMid);
		TempNode=TempNode->_next;
	}
	return sum/count;
}
int* Count(StudentList* First,int _FMid){
	StudentList* TempNode;
	TempNode = First;

	int* result = (int*)malloc(sizeof(int)*5);
	int i;
	for (i = 0; i <5; i++){
		result[i] = 0;
	}
	while(TempNode!=NULL){
		float tmp = Average_Score(TempNode->_node,_FMid);
		if(tmp< 4){
			result[4]++;
		} else{
			if(tmp <6){
				result[3]++;
			}else{
				if(tmp < 8){
					result[2]++;
				} else{
					if(tmp <=9){
						result[1]++;
					} else{
						result[0]++;
					}
				}
			}
		}
		TempNode = TempNode->_next;
	}
	return result;	
}
void Print(StudentList *First){
	StudentList* TempNode;
	TempNode = First;
	while(TempNode!=NULL){
		printf("%s-%s-%s-%s-%.1f-%.1f\n",TempNode->_node._id,TempNode->_node._firstName,TempNode->_node._lastName,TempNode->_node._score,TempNode->_node._midScore,TempNode->_node._finalScore);
		TempNode = TempNode->_next;
	}

}
int Position(StudentList* First,char* StudentId){
	StudentList* TempNode;
	TempNode = First;
	int count = 0;
	while(TempNode != NULL){
		if(strcmp(TempNode->_node._id,StudentId)==0){
			return count;
		}
		count++;
		TempNode = TempNode->_next;
	}
	return -1;
}

int IsEmpty(StudentList *First){
	return !First;
}
StudentList* MakeNull(StudentList* First){
	while(!IsEmpty(First))
		First = Delete_Head(First);
	return First;
}
