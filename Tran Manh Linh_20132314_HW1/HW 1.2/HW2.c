#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "HW2.h"

int menu(){
	int choosedItem=0;
	printf("\n\nLearning Management System\n");
	printf("-------------------------------------\n");
	printf("1.\tAdd a new score board\n");
	printf("2.\tAdd score\n");
	printf("3.\tRemove score\n");
	printf("4.\tSearch score\n");
	printf("5.\tDisplay score board and score report\n");
	printf("Your choise (1-5, other to quit): ");
	scanf("%d",&choosedItem);
	return choosedItem;
}
void handle_choosedItem(){
	char* token = (char*)malloc(3*sizeof(char));
	while (1){
		int i = menu();
		do{
			switch(i){
				case 1:{
					Add_New_Score_Board();
					break;
				}
				case 2:{
					Add_Score();
					break;
				}
				case 3:{
					Search_Or_Delete_Score(1);
					break;
				}
				case 4:{
					Search_Or_Delete_Score(0);
					break;
				}
				case 5:{
					Display_Score();
					break;
				}
				default:{
					printf("Wrong\n");
					return;
				}
			}
			printf("\nContinue with this step? ");
			if(i == 1 || i ==2){
				while(getchar()!= '\n');
			}
			fgets(token,3,stdin);
			token[strlen(token) - 1] = '\0';
		}while(strcmp(token,"y") == 0 || strcmp(token,"Y") == 0);
	}
	
}
void Add_New_Score_Board(){
	char* _SubjectId = (char*)malloc(20);
	char* _Subject = (char*) malloc(50);
	int _Semester;
	int _StudentCount;
	int _FMid,_FFinal;
	printf("\n\nInput Subject Information:\n");
	while(getchar()!='\n');
	
	printf("\tSubjectID: ");
	fgets(_SubjectId,10,stdin);
	if(_SubjectId[strlen(_SubjectId)-1] == '\n'){
		_SubjectId[strlen(_SubjectId)-1] = '\0';
	}

	printf("\tSubject: ");
	fgets(_Subject,40,stdin);
	if(_Subject[strlen(_Subject) - 1] == '\n'){
		_Subject[strlen(_Subject) -1 ] = '\0';
	}
	printf("\tF: (Mid + Final =100)\n");
	do{
		printf("\t\tMid: ");
		scanf("%d",&_FMid);
		printf("\t\tFinal: " );
		scanf("%d",&_FFinal);
		if((_FMid + _FFinal) != 100){
			printf("\tAgain!\n");
		}
	} while((_FMid + _FFinal) !=100);
	
	while(getchar()!='\n');
	printf("\tSemester:");
	scanf("%d",&_Semester);

	do{
		printf("Input StudentCount:");
		scanf("%d",&_StudentCount);
	} while (_StudentCount < 0);

	char* str_Semester = (char*)malloc(16);
	char* str_FMid = (char*)malloc(16);
	char* str_FFinal = (char*)malloc(16);
	char* str_StudentCount = (char*) malloc(16);
	snprintf(str_Semester,16,"%d", _Semester);
	snprintf(str_FMid,16,"%d", _FMid);
	snprintf(str_FFinal,16,"%d",_FFinal);
	snprintf(str_StudentCount,16,"%d",_StudentCount);

	Store_New_Score_In_File(_SubjectId,_Subject,str_FMid,str_FFinal,str_Semester,str_StudentCount);

	free(_SubjectId);
	free(_Subject);
	free(str_Semester);
	free(str_FMid);
	free(str_FFinal);
	free(str_StudentCount);
}
void Store_New_Score_In_File(char* _SubjectId,char* _Subject,char* _FMid,char* _FFinal,char* _Semester,char* _StudentCount){
	FILE *f;
	char* fileName = (char* ) malloc(1+ strlen(_SubjectId) + strlen(_Semester));
	strcpy(fileName,_SubjectId);
	strcat(fileName,"_");
	strcat(fileName,_Semester);
	strcat(fileName,".txt");
	f=fopen(fileName,"w+");
	fputs("SubjectID|",f);
	fputs(_SubjectId,f);
	fputs("\nSubject|",f);
	fputs(_Subject,f);
	fputs("\nF|",f);
	fputs(_FMid,f);
	fputs("|",f);
	fputs(_FFinal,f);
	fputs("\nSemester|",f);
   	fputs(_Semester, f);
   	fputs("\nStudentCount|",f);
   	fputs(_StudentCount,f);
   	fputs("\n",f);
   	free(fileName);
   	fclose(f);
}
char* Get_FileName(){
	char* _SubjectId = (char*) malloc (20);
	char* _Semester = (char*)malloc(15);

	printf("\n\nInput File Information: \n");
	while(getchar()!='\n');

	printf("\tSubjectID:\t");
	fgets(_SubjectId,15,stdin);
	if(_SubjectId[strlen(_SubjectId) - 1] == '\n'){
		_SubjectId[strlen(_SubjectId) -1 ] = '\0';
	}

	printf("\tSemester:\t");
	fgets(_Semester,10,stdin);
	if(_Semester[strlen(_Semester) - 1] == '\n'){
		_Semester[strlen(_Semester)-1] = '\0';
	}

	char* fileName = (char*)malloc(1+ strlen(_Semester) + strlen(_SubjectId));
	strcpy(fileName,_SubjectId);
	strcat(fileName,"_");
	strcat(fileName,_Semester);
	strcat(fileName,".txt");
	return fileName;
}
void Add_Score(){
	char* fileName = Get_FileName();

	FILE* f;
	f=fopen(fileName,"r+");
	if(f == NULL){
		printf("Can't Open File %s",fileName);
		return;
	}	

	StudentList* _first = Get_StudentList(fileName);


	StudentInfo tmp;
	printf("Input Student Information: \n");
	
	printf("\tStudentId: ");
	tmp._id = (char*)malloc(sizeof(char)*10);
	fgets(tmp._id,10,stdin);
	tmp._id[strlen(tmp._id) -1] = '\0';

	printf("\tLastName: ");
	tmp._lastName = (char*)malloc(sizeof(char) * 20);
	fgets(tmp._lastName,20,stdin);
	tmp._lastName[strlen(tmp._lastName) -1] = '\0';

	printf("\tFirst Name: ");
	tmp._firstName = (char*)malloc(sizeof(char)*10);
	fgets(tmp._firstName,10,stdin);
	tmp._firstName[strlen(tmp._firstName) -1] ='\0';

	do{
		printf("\tMiddle Score: ");
		scanf("%f",&tmp._midScore);
	}while (tmp._midScore < 0 || tmp._midScore > 10);

	do{
		printf("\tFinal Score: ");
		scanf("%f",&tmp._finalScore);
	}while (tmp._finalScore < 0 || tmp._finalScore > 10);

	float _score;
	_score = (tmp._midScore * atoi(_FMid) + tmp._finalScore * atoi(_FFinal))/100;
	if(tmp._midScore <3){
		tmp._score = "F";
	}
	else{
		if(_score < 4){
			tmp._score = "F";
		} else{
			if(_score <6){
				tmp._score = "D";
			} else{
				if(_score <8){
					tmp._score = "C";
				} else {
					if(_score <= 9){
						tmp._score = "B";
					}else{
						tmp._score = "A";
					}
				}
			}
		}
	}
	if(_first != NULL){
		_first = Insert_Middle(_first,tmp);
	}
	else{
		_first = Insert_ToHead(_first,tmp);
	}

	Store_In_File(fileName,_first);
}

void Search_Or_Delete_Score(int searchOrDelete){
	char* fileName = Get_FileName();
	StudentList* _first;
	_first = Get_StudentList(fileName);
	if(_first == NULL){
		return;
	}
	char* StudentId = (char*)malloc(10*sizeof(char));
	printf("Input StudentId: ");
	fgets(StudentId,10,stdin);
	StudentId[strlen(StudentId)-1] = '\0';
	int position = Position(_first,StudentId);
	if( position == -1){
		printf("Don't have this student\n");
		return;
	}else{
		if(searchOrDelete == 1){
			_first = Delete_Middle(_first,StudentId);
		} else{
			StudentInfo tmp;
			tmp = Search(_first,StudentId);
			printf("\n\tMiddle Score = %.1f\n\tFinal Score = %.1f\n\tScore = %s",tmp._midScore,tmp._finalScore,tmp._score );
			return;
		}
	}
	Store_In_File(fileName,_first);
}
StudentList* Get_StudentList(char* fileName){

	FILE *f;
	f = fopen(fileName,"r+");
	if(f == NULL){
		printf("Can't Open File %s",fileName);
		return NULL;
	}	
	_FMid = (char*)malloc(3*sizeof(char));
	_FFinal = (char*)malloc(3*sizeof(char));
	char* line = (char*) malloc(255);
	int haveStudent =0;

	char* token;
	while(!feof(f)){
		fgets(line,255,f);
		line[strlen(line)-1] = '\0';
		if(line[0] == 'F'){
			strtok(line,"|");
			token = strtok(NULL,"|");
			strcpy(_FMid,token);
			token = strtok(NULL,"|");
			strcpy(_FFinal,token);
			continue;
		} 
		if(line[0] == 'S' && line[1] == '|'){
			haveStudent =1;
			break;
		}
	}

	StudentList *_first = NULL;
	StudentList *_last;
	StudentInfo _node;

	if(haveStudent == 0){
		printf("Don't have any student\n");
		return NULL;
	}
	_node = setNode(line);
	_first = Insert_ToHead(_first,_node);
	_last = _first;
		
	while(!feof(f)){
		fgets(line,255,f);
		line[strlen(line) - 1] = '\0';
		if(line[0] !='S'){
			break;
		}
		_node = setNode(line);
		_last = Insert_Next(_last,_node);
	}	
	fclose(f);
	return _first;
}
void Display_Score(){
	char* fileName = Get_FileName();
	StudentList* _first = Get_StudentList(fileName);
	if(_first == NULL){
		return;
	}

	StudentInfo _nodeMax,_nodeMin;
	_nodeMax = Max(_first,atoi(_FMid));
	_nodeMin = Min(_first,atoi(_FMid));

	fileName = strtok(fileName,".txt");
	strcat(fileName,"_rp.txt");

	FILE*f;
	f = fopen(fileName,"w+");
	if(f == NULL){
		return;
	}

	fprintf(f, "The student with the highest mark is: %s %s\n",_nodeMax._lastName,_nodeMax._firstName);
	printf("\n\nThe student with the highest mark is: %s %s\n",_nodeMax._lastName,_nodeMax._firstName);

	fprintf(f, "The student with the lowest mark is: %s %s\n",_nodeMin._lastName,_nodeMin._firstName);
	printf("The student with the lowest mark is: %s %s\n",_nodeMin._lastName,_nodeMin._firstName);

	fprintf(f, "The average mark is: %.1f\n",Total_Average_Mark(_first,atoi(_FMid)));
	printf("The average mark is: %.1f\n",Total_Average_Mark(_first,atoi(_FMid)));
	
	char* token = strtok(fileName,"_");
	fprintf(f, "\n\nA histogram of the subject %s is:\n",token);
	printf("\n\nA histogram of the subject %s is:\n",token);

	int *result ;
	result= Count(_first,atoi(_FMid));
	int i;
	fprintf(f, "A:");
	printf("A:");

	for (i=0;i<*(result);i++){
		fprintf(f, "*");
		printf("*");
	}

	fprintf(f, "\nB:");
	printf("\nB:");
	for (i=0;i<*(result+1);i++){
		fprintf(f, "*");
		printf("*");
	}

	fprintf(f, "\nC:");
	printf("\nC:");
	for (i=0;i<*(result+2);i++){
		fprintf(f, "*");
		printf("*");
	}

	fprintf(f, "\nD:");
	printf("\nD:");
	for (i=0;i<*(result+3);i++){
		fprintf(f, "*");
		printf("*");
	}

	fprintf(f, "\nF:");
	printf("\nF:");
	for (i=0;i<*(result+4);i++){
		fprintf(f, "*");
		printf("*");
	}

	fclose(f);
}
void Store_In_File(char* _fileName,StudentList*_first){
	FILE* f;

	f=fopen(_fileName,"r+");
	char* line1 = (char*) malloc(255);
	char* line2 = (char*) malloc(255);
	char* line3 = (char*) malloc(255);
	char* line4 = (char*) malloc(255);
	char* line5 = (char*) malloc(255);
	fgets(line1,255,f);
	fgets(line2,255,f);
	fgets(line3,255,f);
	fgets(line4,255,f);
	fgets(line5,255,f);
	fclose(f);
	f = fopen(_fileName,"w+");
	fputs(line1,f);
	fputs(line2,f);
	fputs(line3,f);
	fputs(line4,f);
	fputs(line5,f);

	char* str_midScore = (char*)malloc(16);
	char* str_finalScore = (char*)malloc(16);

	while(_first != NULL){

		snprintf(str_midScore,16,"%.1f", _first->_node._midScore);
		snprintf(str_finalScore,16,"%.1f",_first->_node._finalScore);

		fputs("S|",f);
		fputs(_first->_node._id,f);
		fputs("|",f);
		fputs(_first->_node._lastName,f);
		fputs("|",f);
		fputs(_first->_node._firstName,f);
		fputs("|",f);
		fputs(str_midScore,f);
		fputs("|",f);
		fputs(str_finalScore,f);
		fputs("|",f);
		fputs(_first->_node._score,f);
		fputs("|\n",f);
		_first = _first->_next;
	}
	free(str_midScore);
	free(str_finalScore);
	fclose(f);
}

StudentInfo setNode(char* _line){
	StudentInfo _Node;
	char* token;
	strtok(_line,"|");

	token = strtok(NULL,"|");
	_Node._id = (char*)malloc(strlen(token)*sizeof(char));
	strcpy(_Node._id,token);

	token = strtok(NULL,"|");
	_Node._lastName = (char*)malloc(strlen(token)*sizeof(char));
	strcpy(_Node._lastName,token);

	token = strtok(NULL,"|");
	_Node._firstName = (char*)malloc(strlen(token)*sizeof(char));
	strcpy(_Node._firstName,token);

	_Node._midScore = atof(strtok(NULL,"|"));
	_Node._finalScore = atof(strtok(NULL,"|"));

	token = strtok(NULL,"|");
	_Node._score = (char*) malloc(strlen(token)*sizeof(char));
	strcpy(_Node._score,token);

	return _Node;
	
}

void main(){
	handle_choosedItem();
}