#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "LinkList.h"

int menu();
void handle_choosedItem();
void Store_New_Score_In_File(char* _SubjectId,char* _Subject,char* _Semester,char* _FMid,char* _FFinal,char* _StudentCount);
void Add_New_Score_Board();
void Add_Score();
void Search_Or_Delete_Score(int searchOrDelete);
void Display_Score();
void Store_In_File(char* _fileName,StudentList*_first);
StudentList* Get_StudentList(char* fileName);
StudentInfo setNode(char* _line);
char* Get_FileName();

char* _FMid;
char* _FFinal;