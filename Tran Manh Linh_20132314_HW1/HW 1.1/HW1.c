#include <stdio.h>
#include <string.h>
#include <stdbool.h>

char input[32];
struct info
{
	char* material;
	char* reactivity;
	char* protection;
	char* containment;
	char* evacuation;
}result;

int check_length(char mInput[]);
void Get_information();
char* Get_Meterial(char mInput[]);
char* Get_Reactivity(char mInput[]);
char* Get_Protection(char mInput[]);
char* Get_Protection_Coloured(char mInput[]);
char* Get_Containment(char mInput[]);
char* Get_Evacuation();
void Set_result(char mInput[],char mColoured[]);
void Show_Information(int style);
void Show_Error();

void main(){
	Get_information();
}

int check_length(char mInput[]){
	if(strlen(mInput) <3 || strlen(mInput) > 4){
		return 3;
	}
	if(mInput[0] < 49 || mInput[0] > 52){
		return 3;
	}
	if(mInput[1] != 'P' && mInput[1] != 'R' && mInput[1] != 'S' && mInput[1] != 'T' && mInput[1] != 'W' && mInput[1] != 'X' && mInput[1] != 'Y' && mInput[1] != 'Z'){
		return 3;
	}
	if(strlen(mInput) == 4){
		if(mInput[2] != 'E')
			return 3;
	}
	if(mInput[1] == 'S' || mInput[1] == 'T' || mInput[1] == 'Y' || mInput[1] == 'Z'){
		return 2;
	}
	return 1;
}

void Get_information(){
	
	printf("Enter HAZCHEM code: ");
	fgets(input,8,stdin);
	int i = check_length(input);
	switch(i){
		case 3:{
			Show_Error();
			break;
		}
		case 2:{
			printf("Is the %c reverse coloured? (yes/no) ",input[1]);
			char coloured[5];
			do{
				fgets(coloured,4,stdin);
				if(strcmp(coloured,"yes")!=0 && strcmp(coloured,"no\n")!=0){
					while(getchar()!='\n');
					printf("\nWrong, Input again: ");
				}
			}
			while (strcmp(coloured,"yes")!=0 && strcmp(coloured,"no\n")!=0);
			Set_result(input,coloured);
			return;
		}
		case 1:{
			Set_result(input,"no");
			break;
		}

	}
}
char* Get_Meterial(char mInput[]){
	switch(mInput[0]){
		case '1':{
			return "jets";
		}
		case '2':{
			return "fog";
		}
		case '3':{
			return "foam";
		}
		case '4':{
			return "dry agent";
		}
	}
}
char* Get_Reactivity(char mInput[]){
	switch(mInput[1]){
		case 'P':
		case 'S':
		case 'W':
		case 'Y':
		case 'Z':{
			return "can be violently reactive";
		}
		default:{
			return "normal reactive";
		}
	}
}

char* Get_Protection(char mInput[]){
	switch(mInput[1]){
		case 'P':
		case 'R':
		case 'W':
		case 'X':{
			return "full protection with clothes and mask";
		}
		default:{
			return "breathing apparatus, protective gloves";
		}
	}
}

char* Get_Protection_Coloured(char mInput[]){
	return "breathing apparatus, protective gloves for fire only";
}

char* Get_Containment(char mInput[]){
	switch(mInput[1]){
		case 'P':
		case 'R':
		case 'S':
		case 'T':{
			return "may be diluted and washed down the drain";
		}
		default:{
			return "chemicals should be stored in containers . Not for exposure to chemicals and water";
		}
	}
}

char* Get_Evacuation(){
	return "consider evacuation";
}

void Set_result(char mInput[],char mColoured[]){
	result.material = Get_Meterial(mInput);
	result.reactivity = Get_Reactivity(mInput);
	if(strcmp(mColoured,"yes")==0){
		result.protection = Get_Protection_Coloured(mInput);
	} else{
		result.protection = Get_Protection(mInput);
	}
	result.containment = Get_Containment(mInput);
	if(strlen(mInput) == 4){
		result.evacuation = Get_Evacuation();
		Show_Information(1);
		return;
	}
	Show_Information(0);
}

void Show_Information(int style){
	printf("\n\n\n***Emergency action advice***\n");
	printf("Material:\t%s\n",result.material);
	printf("Reactivity:\t%s\n",result.reactivity);
	printf("Protection:\t%s\n",result.protection);
	printf("Containment:\t%s\n",result.containment);
	if(style == 1){
		printf("Evacuation:\t%s\n",result.evacuation);
	}
	printf("*****************************\n\n\n");
}
void Show_Error(){
	printf("Wrong code\n");
}
