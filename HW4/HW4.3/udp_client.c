#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
/*
101 - not found userid
102 - found userid
103 - deactived account
104 - match userid and passwd
105 - logout_request
106 - logout
107 - wrong request code
108 - request client retry passwd
*/

int main(){
	int client_sock;
	char buff[1024];
	struct sockaddr_in server_addr;
	int bytes_sent,bytes_received, sin_size;

	client_sock=socket(AF_INET,SOCK_DGRAM,0);

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(5550);
	server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	
	sin_size = sizeof(struct sockaddr);	

	char* reply = (char*) malloc(sizeof(char) *1024);
	printf("\nInsert userid:");
	memset(buff,'\0',(strlen(buff)+1));
	gets(buff);

	//send userid
	bytes_sent = sendto(client_sock,buff,strlen(buff),0, (struct sockaddr *) &server_addr, sin_size);
	if(bytes_sent == -1){
		printf("\nError!Cannot send data to sever!\n");
		close(client_sock);
		exit(-1);
	}

	bytes_received = recvfrom(client_sock,buff,1024,0, (struct sockaddr *) &server_addr, &sin_size);
	if(bytes_received == -1){
		printf("\nError!Cannot receive data from sever!\n");
		close(client_sock);
		exit(-1);
	}
	buff[bytes_received] = '\0';
	reply = &buff[0];
	if(strcmp(reply,"101") == 0){
		printf("Wrong userid, close!\n");
		close(client_sock);
		return 0;
	}
	if(strcmp(reply,"102") !=0){
		printf("Wrong reply\n");
		close(client_sock);
		return 0;
	}

	do{
		printf("\nInsert passwd:");
		memset(buff,'\0',(strlen(buff)+1));
		gets(buff);

		//send userid
		bytes_sent = sendto(client_sock,buff,strlen(buff),0, (struct sockaddr *) &server_addr, sin_size);
		if(bytes_sent == -1){
			printf("\nError!Cannot send data to sever!\n");
			close(client_sock);
			exit(-1);
		}

		bytes_received = recvfrom(client_sock,buff,1024,0, (struct sockaddr *) &server_addr, &sin_size);
		if(bytes_received == -1){
			printf("\nError!Cannot receive data from sever!\n");
			close(client_sock);
			exit(-1);
		}
		buff[bytes_received] = '\0';
		reply = &buff[0];
		if(strcmp(reply,"104") == 0){
			//authenticated
			printf("login\n");
			break;
		} else{
			if(strcmp(reply,"103") == 0){
				// not match passwd and retry_times > max
				printf("Got wrong password for 5 times, close\n");
				close(client_sock);
				return 0;
			}
		}
	} while(strcmp(reply,"108") == 0);

	do{
		printf("\nWant to logout [Y/N]: ");
		memset(buff,'\0',(strlen(buff)+1));
		gets(buff);
	} while(strcmp(buff,"Y") != 0 && strcmp (buff,"y") != 0);
	do{
		bytes_sent = sendto(client_sock,"105",4,0, (struct sockaddr *) &server_addr, sin_size);
		if(bytes_sent == -1){
			printf("\nError!Cannot send data to sever!\n");
			close(client_sock);
			exit(-1);
		}

		bytes_received = recvfrom(client_sock,buff,1024,0, (struct sockaddr *) &server_addr, &sin_size);
		if(bytes_received == -1){
			printf("\nError!Cannot receive data from sever!\n");
			close(client_sock);
			exit(-1);
		}
		buff[bytes_received] = '\0';
		reply = &buff[0];
	}while(strcmp(reply,"106") != 0);
	printf("Bye!\n");
	close(client_sock);
	return 0;
}
