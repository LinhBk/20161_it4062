#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#define PORT 5550  /* Port that will be opened */ 
/*
101 - not found userid
102 - found userid
103 - deactived account
104 - match userid and passwd
105 - logout_request
106 - logout
107 - wrong request code
108 - request client retry passwd
*/
struct account
{
	/* data */
	char* userid;
	char* passwd;
	struct account* next;
};

struct account *first,*last;

int Get_Data();
void Insert_ToFirst(char* userid,char* passwd);
void Insert_ToLast(char* userid,char* passwd);
char* Search(char* userid_received);
int main()
{
 	if(Get_Data() == -1){
 		printf("Error!Cannot get data!\n");
 		exit(-1);
 	}

	int server_sock; /* file descriptors */
	char recv_data[1024];
	int bytes_sent, bytes_received;
	struct sockaddr_in server; /* server's address information */
	struct sockaddr_in client; /* client's address information */
	int sin_size;


	if ((server_sock=socket(AF_INET, SOCK_DGRAM, 0)) == -1 ){  /* calls socket() */
		printf("socket() error\n");
		exit(-1);
	}

	server.sin_family = AF_INET;         
	server.sin_port = htons(PORT);   /* Remember htons() from "Conversions" section? =) */
	server.sin_addr.s_addr = INADDR_ANY;  /* INADDR_ANY puts your IP address automatically */   
	bzero(&(server.sin_zero),8); /* zero the rest of the structure */

  
	if(bind(server_sock,(struct sockaddr*)&server,sizeof(struct sockaddr))==-1){ /* calls bind() */
		printf("bind() error\n");
		exit(-1);
	}     


	while(1){
		sin_size=sizeof(struct sockaddr_in);
    	
    	//status unathenticated
    	bytes_received = recvfrom(server_sock,recv_data,1024,0,(struct sockaddr*)&client, &sin_size);
		if(bytes_received == -1){
			printf("\nError!Cannot receive data from client!\n");
			continue;
		} else{
			recv_data[bytes_received] = '\0';
		}
		char* userid_received = (char*)malloc(sizeof(char)*50);
		strcpy(userid_received,&recv_data[0]);
		char* passwd = (char*)malloc(sizeof(char)*50); 
		passwd= Search(userid_received);

		if(strcmp(passwd, " ") == 0){
			//not found userId
			bytes_sent = sendto(server_sock,"101",4,0,(struct sockaddr*)&client, sin_size);
			if(bytes_sent == -1){
				printf("\nError!Cannot send data to client!\n");
				continue;
			}
			continue;
		}

		//found userid
		bytes_sent = sendto(server_sock,"102",4,0,(struct sockaddr*) &client, sin_size);
		if(bytes_sent == -1){
			printf("\nError!Cannot send data to client!\n");
			continue;
		}
		//specifiedID
		int retry_time =0;
		while (retry_time <5){
			bytes_received = recvfrom(server_sock,recv_data,1024,0,(struct sockaddr*)&client,&sin_size);
			if(bytes_received == -1){
				printf("\nError!Cannot receive data from client!\n");
				continue;
			} else{
				recv_data[bytes_received] = '\0';
			}

			char* passwd_received = &recv_data[0];
			if(strcmp(passwd,passwd_received) == 0){
				//authenticated
				bytes_sent =sendto(server_sock,"104",4,0,(struct sockaddr*)&client, sin_size);
				if(bytes_sent == -1){
					printf("\nError!Cannot send data to client!\n");
					continue;
				}
				break;
			} else{
				retry_time ++;
				if(retry_time == 5){
					// not match passwd and retry_times > max
					bytes_sent =sendto(server_sock,"103",4,0,(struct sockaddr*)&client, sin_size);
					if(bytes_sent == -1){
						printf("\nError!Cannot send data to client!\n");
						continue;
					}			
				}
				bytes_sent =sendto(server_sock,"108",4,0,(struct sockaddr*)&client, sin_size);
				if(bytes_sent == -1){
					printf("\nError!Cannot send data to client!\n");
					continue;
				}
				
			}
				
		}
		if(retry_time == 5){
			continue;
		}
		printf("\nlogin account %s\n",userid_received);

		// logout
		while(1){
			bytes_received = recvfrom(server_sock,recv_data,1024,0,(struct sockaddr*)&client ,&sin_size);
			if(bytes_received == -1){
				printf("\nError!Cannot receive data from client!\n");
				break;
			} else{
				recv_data[bytes_received] = '\0';
			}
			char* request = &recv_data[0];
			if(strcmp(request,"105") == 0){
				printf("logout account %s\n",userid_received);
				bytes_sent =sendto(server_sock,"106",4,0,(struct sockaddr*)&client, sin_size);
				if(bytes_sent == -1){
					printf("\nError!Cannot send data to client!\n");
					break;
				}
				break;
			} else{
				bytes_sent =sendto(server_sock,"107",4,0,(struct sockaddr*)&client, sin_size);
				if(bytes_sent == -1){
					printf("\nError!Cannot send data to client!\n");
					break;
				}
			}
		}			
	}
	
	close(server_sock);
	return 0;
}

void Insert_ToLast(char* userid,char* passwd){
	struct account *newNode;
	newNode = (struct account*)malloc(sizeof(struct account));
	newNode->userid = (char*)malloc(sizeof(char));
	newNode->passwd = (char*)malloc(sizeof(char));
	strcpy(newNode->userid,userid);
	strcpy(newNode->passwd,passwd);
	last->next = newNode;
	last = newNode;
}

void Insert_ToFirst(char* userid,char* passwd){
	first = (struct account*)malloc(sizeof(struct account*));
	last = (struct account*) malloc(sizeof(struct account*));
	first->userid = (char*)malloc(sizeof(char));
	first->passwd = (char*)malloc(sizeof(char));
	strcpy(first->userid,userid);
	strcpy(first->passwd,passwd);
	first->next = NULL;
	
	last = first;
}

char* Search(char* userid_received){
	struct account* tempNode;
	tempNode = (struct account*) malloc(sizeof(struct account));
	tempNode = first;
	do{
		if(strcmp(tempNode->userid,userid_received) == 0){
			return tempNode->passwd;
		}
		tempNode = tempNode->next;
	} while(tempNode != NULL);
	return " ";
}
int Get_Data(){
	FILE *f;
	f = fopen("account_info.txt","r");
	if(f == NULL){
		printf("Cannot open file.\n");
		return -1;
	}
	char* buff;
	char* userid;
	char* passwd;
	userid = (char*) malloc(sizeof(char)*50);
	passwd = (char*)malloc(sizeof(char)*50);
	buff = (char*) malloc(sizeof(char)*1024);
	fgets(buff,1024,f);
	fgets(buff,1024,f);

	if(buff[strlen(buff)-1] == '\n'){
		buff[strlen(buff)-1] = '\0';
	}
	userid = strtok(buff," ");
	passwd = strtok(NULL," ");
	Insert_ToFirst(userid,passwd);

	while(!feof(f)){
		fgets(buff,1024,f);
		if(buff[strlen(buff)-1] == '\n'){
			buff[strlen(buff)-1] = '\0';
		}
		userid = strtok(buff," ");
		passwd = strtok(NULL, " ");
		Insert_ToLast(userid,passwd);	
	}
	return 1;
}