#include <stdio.h>          /* These are the usual header files */
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

#define PORT 5550  /* Port that will be opened */ 
void Conver_char(char* input){
	int i =0;
	while(input[i] != 0){
		if(isalpha(input[i])){
			if(input[i] >=97){
				input[i] = input[i] -32;
			}
		}
		i++;
	}
}
int main()
{
 
	int server_sock; /* file descriptors */
	char recv_data[1024];
	int bytes_sent, bytes_received;
	struct sockaddr_in server; /* server's address information */
	struct sockaddr_in client; /* client's address information */
	int sin_size;


	if ((server_sock=socket(AF_INET, SOCK_DGRAM, 0)) == -1 ){  /* calls socket() */
		printf("socket() error\n");
		exit(-1);
	}

	server.sin_family = AF_INET;         
	server.sin_port = htons(PORT);   /* Remember htons() from "Conversions" section? =) */
	server.sin_addr.s_addr = INADDR_ANY;  /* INADDR_ANY puts your IP address automatically */   
	bzero(&(server.sin_zero),8); /* zero the rest of the structure */

  
	if(bind(server_sock,(struct sockaddr*)&server,sizeof(struct sockaddr))==-1){ /* calls bind() */
		printf("bind() error\n");
		exit(-1);
	}     

	while(1){
		sin_size=sizeof(struct sockaddr_in);
 		FILE *f;
 		f = fopen("server_temp.txt","w");
 		if(f == NULL){
 			printf("Can't open file \n");
 			close(server_sock);
 			return 0;
 		}

 		bytes_received = recvfrom(server_sock,recv_data,1024,0, (struct sockaddr *) &client, &sin_size);
		
		if (bytes_received < 0){
			printf("\nError!Can not receive data from client!");
			close(server_sock);
			exit(-1);
		}
		else{
			recv_data[bytes_received] = '\0';
		}

		int dataLength = atoi(recv_data);
		int nLeft = dataLength;
		while(nLeft > 0){
			bytes_received = recvfrom(server_sock,recv_data,1024,0, (struct sockaddr *) &client, &sin_size);
			
			if (bytes_received < 0){
				printf("\nError!Can not receive data from client!");
				close(server_sock);
				exit(-1);
			}
			else{
				recv_data[bytes_received] = '\0';
				Conver_char(recv_data);
			}
			fprintf(f, "%s",recv_data);
			nLeft -= bytes_received;
			
			strcpy(recv_data,"OK");
			bytes_sent = sendto(server_sock,recv_data,strlen(recv_data),0,(struct sockaddr*) &client,sin_size);
			if(bytes_sent == -1){
				printf("\nError!Can not send data from client!\n");
				close(server_sock);
				exit(-1);
			}
		}

		fclose(f);	
		
		nLeft = dataLength;
		f = fopen("server_temp.txt","r");
		if(f == NULL){
			printf("Can't open file.\n");
			close(server_sock);
			exit(-1);
		}
		while(nLeft >0){
			fgets(recv_data, 1024,f);
			bytes_sent = sendto(server_sock,recv_data,strlen(recv_data), 0,(struct sockaddr*) &client,sin_size);
			if(bytes_sent == -1){
				printf("\nError!Can not send data from client!\n");
				close(server_sock);
				exit(-1);
			}
			nLeft -= bytes_sent;

			bytes_received = recvfrom(server_sock,recv_data,1024,0,(struct sockaddr*)&client,&sin_size);
			if(bytes_received == -1){
				printf("Error!Can not receive data from client!\n");
				close(server_sock);
				exit(-1);
			}

		}
		fclose(f);
		printf("Done with client %s.\n",inet_ntoa(client.sin_addr));
	}
	
	close(server_sock);
	return 0;
}
