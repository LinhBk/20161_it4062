#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

int fsize(FILE *f)
{
    int size;
    fseek(f, 0, SEEK_END);
    size = (int)ftell(f);
    rewind(f);
    return size;
}
void setResultDirector(char* input){
	input = strtok(input,".txt");
	strcat(input,"_result.txt");
}
int main(){
	int client_sock;
	char buff[1024];
	char* fileName;
	fileName = (char*)malloc(sizeof(char)*1024);

	struct sockaddr_in server_addr;
	int bytes_sent,bytes_received, sin_size;

	client_sock=socket(AF_INET,SOCK_DGRAM,0);

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(5550);
	server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	
	printf("\nInsert file director: ");
	memset(fileName,'\0',(strlen(fileName)+1));
	gets(fileName);
	FILE *f;
	f = fopen(fileName,"r");
	if(f == NULL){
		printf("Can't open file.\n");
		close(client_sock);
		return 0;
	}

	int dataLength = fsize(f);
	int nLeft = dataLength;


	sin_size = sizeof(struct sockaddr);
	
	sprintf(buff, "%d", dataLength);
	bytes_sent = sendto(client_sock,buff,strlen(buff),0, (struct sockaddr *) &server_addr, sin_size);
	if(bytes_sent == -1){
		printf("\nError!Cannot send data to server!\n");
		close(client_sock);
		exit(-1);
	}

	while(nLeft >0){
		fgets(buff, 1024,f);
		bytes_sent = sendto(client_sock,buff,strlen(buff), 0,(struct sockaddr*) &server_addr,sin_size);
		if(bytes_sent == -1){
			printf("\nError!\n");
			close(client_sock);
			exit(-1);
		}
		nLeft -= bytes_sent;

		bytes_received = recvfrom(client_sock,buff,1024,0,(struct sockaddr*)&server_addr,&sin_size);
		if(bytes_received == -1){
			printf("Error\n");
			close(client_sock);
			exit(-1);
		}
	}

	fclose(f);
	setResultDirector(fileName);
	f = fopen(fileName,"w+");
	if(f == NULL){
		printf("Can't open file\n");
		close(client_sock);
		return 0;
	}
	nLeft = dataLength;
	while(nLeft > 0){
		bytes_received = recvfrom(client_sock,buff,1024,0, (struct sockaddr *) &server_addr, &sin_size);
		
		if (bytes_received < 0){
			printf("\nError!Can not receive data from sever!");
			close(client_sock);
			exit(-1);
		}
		else{
			buff[bytes_received] = '\0';
		}
		fprintf(f, "%s",buff);
		nLeft -= bytes_received;
		strcpy(buff,"OK");
		bytes_sent = sendto(client_sock,buff,strlen(buff),0,(struct sockaddr*) &server_addr,sin_size);
		if(bytes_sent == -1){
			printf("Error\n");
			close(client_sock);
			exit(-1);
		}
	}
	fclose(f);
	printf("Total bytes sent : %d\n",dataLength );
	close(client_sock);
	return 0;
}
