#include <stdio.h>          /* These are the usual header files */
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

#define PORT 5550   /* Port that will be opened */ 
#define BACKLOG 2   /* Number of allowed connections */
void Convert_Char(char* input){
	int i =0;
	while(input[i] != 0){
		if(isalpha(input[i])){
			if(input[i] >= 97){
				input[i] -= 32;
			}
		}
		i++; 
	}
	
}
void main()
{
 
	int listen_sock, conn_sock; /* file descriptors */
	char recv_data[1024];
	int bytes_sent, bytes_received;
	struct sockaddr_in server; /* server's address information */
	struct sockaddr_in client; /* client's address information */

	int sin_size;

	if ((listen_sock=socket(AF_INET, SOCK_STREAM, 0)) == -1 ){  /* calls socket() */
		printf("socket() error\n");
		exit(-1);
	}
	bzero(&server, sizeof(server));
	server.sin_family = AF_INET;         
	server.sin_port = htons(PORT);   /* Remember htons() from "Conversions" section? =) */
	server.sin_addr.s_addr = htonl(INADDR_ANY);  /* INADDR_ANY puts your IP address automatically */   

	if(bind(listen_sock,(struct sockaddr*)&server,sizeof(server))==-1){ /* calls bind() */
		printf("bind() error\n");
		exit(-1);
	}     

	if(listen(listen_sock,BACKLOG) == -1){  /* calls listen() */
		printf("listen() error\n");
		exit(-1);
	}

	while(1){
		sin_size=sizeof(struct sockaddr_in);
		if ((conn_sock = accept(listen_sock,(struct sockaddr *)&client,&sin_size))==-1){ /* calls accept() */
			printf("accept() error\n");
			exit(-1);
		}
  
		printf("You got a connection from %s\n",inet_ntoa(client.sin_addr) ); /* prints client's IP */
  
		bytes_sent = send(conn_sock,"Welcome to my server.\n",22,0); /* send to the client welcome message */
		if (bytes_sent < 0){
			printf("\nError!Can not sent data to client!");
			close(conn_sock);
			continue;
		}
		
		bytes_received = recv(conn_sock,recv_data,1024,0); //blocking
		if (bytes_received < 0){
			printf("\nError!Can not receive data from client!");
			close(conn_sock);
		}
		else{
			recv_data[bytes_received] = '\0';
		}

		strtok(recv_data,"=");
		int dataLength;
		dataLength = atoi(strtok(NULL,"="));
		int nLeft = dataLength;
		int idx =0;
		fflush(stdin);
		char* data;
		data = (char*)malloc(sizeof(char)*dataLength);

		while(nLeft >0){
			bytes_received = recv(conn_sock,&data[idx],1000,0);
			if(bytes_received == -1){
				printf("Error\n");
				close(conn_sock);
				exit(-1);
			}
			nLeft -= bytes_received;
			idx += bytes_received;
			char str[10];
			sprintf(str,"%d",bytes_received);
			bytes_sent = send(conn_sock,str,strlen(str),0);
			if(bytes_sent == -1){
				printf("Error\n");
				close(conn_sock);
				exit(-1);
			}

		}

		nLeft = dataLength;
		idx =0;
		Convert_Char(data);

		while(nLeft >0){
			bytes_sent = send(conn_sock,&data[idx],nLeft,0);
			if(bytes_sent == -1){
				printf("Error\n");
				close(conn_sock);
				exit(-1);
			}
			nLeft -= bytes_sent;
			idx += bytes_sent;
		}
		printf("DONE\n");

	}
	close(listen_sock);
	
}