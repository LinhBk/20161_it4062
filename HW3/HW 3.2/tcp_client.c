#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
int fsize(FILE *f)
{
    int size;
    fseek(f, 0, SEEK_END);
    size = (int)ftell(f);
    rewind(f);
    return size;
}
void main(){
	int client_sock;
	char buff[1024];
	char* data;

	struct sockaddr_in server_addr;
	int bytes_sent,bytes_received;
	FILE*f;

	client_sock=socket(AF_INET,SOCK_STREAM,0);

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(5550);
	server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	if(connect(client_sock,(struct sockaddr*)&server_addr,sizeof(struct sockaddr))!=0){
		printf("\nError!Can not connect to sever!Client exit imediately! ");
		return;
	}
	
	bytes_received = recv(client_sock,buff,1024,0);
	if(bytes_received == -1){
		printf("\nError!Cannot receive data from sever!\n");
		close(client_sock);
		exit(-1);
	}
	buff[bytes_received] = '\0';
	puts(buff);
	
	f = fopen("client.txt","r");
	int size = fsize(f);
	data = (char*)malloc(sizeof(char)*size);
	fgets(buff,1000,f);
	if(buff[strlen(buff) ] == '\n'){
		buff[strlen(buff) ] = '\0';
	}
	strcpy(data,buff);
	while (!feof(f)){
		fgets(buff,1000,f);
		if(buff[strlen(buff) ] == '\n'){
			buff[strlen(buff) ] = '\0';
		}
		strcat(data,buff);
	}
	fclose(f);

	int dataLength = size;
	int nLeft,idx;
	nLeft = dataLength;
	idx =0;

	strcpy(buff,"total bytes sent =");
	char str[10];
	sprintf(str, "%d", dataLength);
	strcat(buff,str);
	bytes_sent = send(client_sock,buff,strlen(buff),0);

	if(bytes_sent == -1){
		printf("\nError!Cannot send data to sever!\n");
		close(client_sock);
		exit(-1);
	}
	
	while(nLeft > 0){
		bytes_sent = send(client_sock,&data[idx], 1000,0);
		if(bytes_sent == -1){
			printf("Error\n");
			close(client_sock);
			exit(-1);
		}
		int tmp;
		bytes_received = recv(client_sock,buff,strlen(buff),0);
		if(bytes_received == -1){
			printf("Error\n");
			close(client_sock);
			exit(-1);
		}
		tmp = atoi(buff);
		nLeft -= tmp;
		idx += tmp;

	}
	nLeft = dataLength;
	idx =0;
	while(nLeft>0){
		bytes_received = recv(client_sock,&data[idx],nLeft,0);
		if(bytes_received == -1){
			printf("Error\n");
			close(client_sock);
			exit(-1);
		}
		nLeft -= bytes_received;
		idx += bytes_received;
		
	}

	FILE * fresult;
	fresult = fopen("result.txt","w");
	if(fresult == NULL){
		printf("Error\n");
		close(client_sock);
		return;
	}
	fprintf(fresult, "%s",data);
	fclose(fresult);
	printf("DONE\n");
	close(client_sock);
}