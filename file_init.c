#include <stdio.h>
#include <stdlib.h>
void main(){
	FILE *f;
	time_t t;
	srand((unsigned) time(&t));
	f = fopen("client.txt","w+");
	if (f == NULL){
		return;
	}
	int i,tmp;
	char ch;
	for (i = 0; i < 6000000;i++){
		tmp = rand()%(2) +1;
		if(tmp == 1){
			tmp = rand()%(122-97) + 97;
			fputc(tmp,f);
		} else{
			tmp = rand()%(90-65) + 65;
			fputc(tmp,f);
		}
	}
	fclose(f);
}