#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
void printInfo(struct hostent* host);	

void main(){
	char* input = (char*) malloc(sizeof(char)*50);
	printf("Nhap ten mien hoac dia chi ip: " );
	fgets(input,50,stdin);
	input[strlen(input) -1] = '\0';
	struct hostent *result;

	struct in_addr someAddr;
	if(inet_aton(input,&someAddr)){
		result = gethostbyaddr(&someAddr, sizeof(someAddr), PF_INET);
		if(result != NULL){
			printf("%s\n",result->h_name );
			return;
		}
		else{
			herror("gethostbyaddr");
			return;
		}
	} else{
		result = gethostbyname(input);
		if(result !=NULL){
			printInfo(result);
			return;

		}
		else{
			herror("gethostbyname");
			return;
		}
	}

}

void printInfo(struct hostent* host){
	struct in_addr **addr_list;
	char** aliases;

	printf("\nOfficial name is: \n\t%s\n", host->h_name );
	
	aliases = (char**)host->h_aliases;
	if(aliases != NULL){
		printf("Other name: \n");
		for(int j = 0;aliases[j] != NULL;j++){
			printf("\t%s\n",aliases[j] );
		}
	}
	printf("IP address: \n\t%s\n",inet_ntoa(*(struct in_addr*)host->h_addr));
	printf("All addresses: \n");
	addr_list = (struct in_addr**)host->h_addr_list;
	for (int i = 0; addr_list[i] != NULL; ++i)
	{
		printf("\t%s\n",inet_ntoa(*addr_list[i]) );
	}
}
